package com.akinola.mindvalley_akinola_olayinka_android_test;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Build;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.akinola.imageloader.ImageLoader;
import com.akinola.mindvalley_akinola_olayinka_android_test.models.Categories;
import com.akinola.mindvalley_akinola_olayinka_android_test.models.Person;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ActivityProfile extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private Animation slide_in_left, slide_in_right, slide_out_left, slide_out_right;
    private ImageView imageView1,imageView2,imageView3;
    private static Person person;
    private static Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        context = this;

        Bundle extras = getIntent().getExtras();
        if (extras!=null){
            person = getIntent().getParcelableExtra("PERSON");
        }

        if (person==null){
            onBackPressed();
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        setUpViewPager();

        setUpCollapsingToolBar();

        DoAnimation();


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

    }

    private void setUpViewPager() {
        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
    }

    private void setUpCollapsingToolBar() {
        final CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsingToolbar);
        collapsingToolbarLayout.setTitleEnabled(false);
    }

    private void DoAnimation(){
        slide_in_left = AnimationUtils.loadAnimation(this, R.anim.pull_in_left);
        slide_in_right = AnimationUtils.loadAnimation(this, R.anim.pull_in_right);
        slide_out_left = AnimationUtils.loadAnimation(this, R.anim.push_out_left);
        slide_out_right = AnimationUtils.loadAnimation(this, R.anim.push_out_right);

        //get reference to the view flipper
        ViewFlipper myViewFlipper = (ViewFlipper) findViewById(R.id.headerViewFlipper);
        //set the animation for the view that enters the screen
        myViewFlipper.setInAnimation(slide_in_right);
        //set the animation for the view leaving th screen
        myViewFlipper.setOutAnimation(slide_out_left);
        myViewFlipper.setFlipInterval(10000);
        myViewFlipper.startFlipping();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_activity_profile, container, false);
            TextView textPersonName = (TextView) rootView.findViewById(R.id.txtPersonName);
            TextView textPersonLikes = (TextView) rootView.findViewById(R.id.txtPersonLikes);
            TextView textPersonCreatedAt = (TextView) rootView.findViewById(R.id.txtPersonCreatedAt);

            TableLayout categoriesTable = (TableLayout)rootView.findViewById(R.id.tableLayoutCategories);

            CircleImageView displayPix = (CircleImageView) rootView.findViewById(R.id.imageViewUserDp);



            MainActivity.imageLoader.withContext(context)
                    .fromUrl(person.getUser().getProfile_image().get("large"))
                    .loadIntoView(displayPix)
                    .withTag(person.getId())
                    .asFileType(ImageLoader.TYPE.IMAGE)
                    .execute();

            textPersonName.setText(person.getUser().getName());
            textPersonLikes.setText(String.valueOf(person.getLikes()));
            textPersonCreatedAt.setText(person.getCreated_at());


            TableRow.LayoutParams vvLayout =new TableRow.LayoutParams(1, ViewGroup.LayoutParams.MATCH_PARENT);
            TableRow.LayoutParams vhLayout =new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,1 );
            TableRow.LayoutParams txtv = new TableRow.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,45);

            List<Categories> categoriesList = person.getCategories();

            int l = categoriesList.size();

            for (int i = 0; i < l; i++) {
                TableRow r = new TableRow(context);
                r.setGravity(Gravity.CENTER_VERTICAL);

                TextView txtCategoryTitle = new TextView(context);
                txtCategoryTitle.setText(categoriesList.get(i).getTitle());
                //t.setLayoutParams(new ViewGroup.LayoutParams(0,45));
                txtCategoryTitle.setBackgroundColor(Color.WHITE);
                txtCategoryTitle.setGravity(Gravity.CENTER_VERTICAL);
                txtCategoryTitle.setPadding(10,0,0,0);
                txtCategoryTitle.setTextColor(Color.BLACK);
                if (Build.VERSION.SDK_INT < 23) {
                    txtCategoryTitle.setTextAppearance(context,R.style.ProfileTextViews);
                } else {
                    txtCategoryTitle.setTextAppearance(R.style.ProfileTextViews);
                }

                View vv = new View(context);
                vv.setBackgroundColor(Color.rgb(200,200,200));
                vv.setLayoutParams(vvLayout);

                TextView txtPhotoCount = new TextView(context);
                txtPhotoCount.setText(String.valueOf(categoriesList.get(i).getPhoto_count()));
                //t.setLayoutParams(new ViewGroup.LayoutParams(0,45));
                txtPhotoCount.setBackgroundColor(Color.WHITE);
                txtPhotoCount.setGravity(Gravity.CENTER_VERTICAL);
                txtPhotoCount.setPadding(10,0,0,0);
                txtPhotoCount.setTextColor(Color.BLACK);
                if (Build.VERSION.SDK_INT < 23) {
                    txtPhotoCount.setTextAppearance(context,R.style.ProfileTextViews);
                } else {
                    txtPhotoCount.setTextAppearance(R.style.ProfileTextViews);
                }

                r.addView(txtCategoryTitle,txtv);
                r.addView(vv);
                r.addView(txtPhotoCount,txtv);
                categoriesTable.addView(r);

                if (i<l-1){
                    View vh = new View(context);
                    vh.setBackgroundColor(Color.rgb(200,200,200));
                    vh.setLayoutParams(vhLayout);

                    categoriesTable.addView(vh);
                }

            }

            return rootView;
        }
    }

    public static class FriendsFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public FriendsFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static FriendsFragment newInstance(int sectionNumber) {
            FriendsFragment fragment = new FriendsFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_another_fragment, container, false);
            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            textView.setText("Friends goes here");
            return rootView;
        }
    }

    /**
     * The places fragment if needed i the tabbed viewis executed
     */
    public static class PlacesFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlacesFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlacesFragment newInstance(int sectionNumber) {
            PlacesFragment fragment = new PlacesFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_places_fragment, container, false);
            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            textView.setText("Places comes here");
            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        //Create dummy Tabhost headers
        String[] sectionHeader = {"Profile Info","Friends","Places"};
        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            switch (position){
                case 0:
                    return PlaceholderFragment.newInstance(position + 1);
                case 1:
                    return FriendsFragment.newInstance(position + 1);
                case 2:
                    return PlacesFragment.newInstance(position + 1);
                default:
                    return PlaceholderFragment.newInstance(position + 1);

            }

        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return sectionHeader.length;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return sectionHeader[position];
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }
}

package com.akinola.mindvalley_akinola_olayinka_android_test;

import android.content.Context;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.akinola.imageloader.ImageLoader;
import com.akinola.mindvalley_akinola_olayinka_android_test.adapters.GridAutofitLayoutManager;
import com.akinola.mindvalley_akinola_olayinka_android_test.adapters.PersonItemRecyclerAdapter;
import com.akinola.mindvalley_akinola_olayinka_android_test.adapters.ServiceHandler;
import com.akinola.mindvalley_akinola_olayinka_android_test.models.Person;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private Context context;

    private static final int TIME_INTERVAL = 2000;
    private long mBackPressed;


    public static ImageLoader imageLoader;

    private SwipeRefreshLayout swipeRefreshLayout;
    Gson gson;

    final String TAG = "MindValley";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        context = this;
        gson = new Gson();

        final int cacheSize = 10000;
        imageLoader = ImageLoader.getInstance(cacheSize);
        PersonItemRecyclerAdapter.persons = new ArrayList<>();

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);

        swipeRefreshLayout.setColorSchemeResources(R.color.color1,
                R.color.color2,
                R.color.color3,
                R.color.color4);

        populateList();

        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewContent);
        adapter = new PersonItemRecyclerAdapter(context);

        //Used a custom gridlayout, totally discretionary.
        //Could have used a StaggeredGridlayoutManager too :)
        GridAutofitLayoutManager gridLayoutManager = new GridAutofitLayoutManager(context, 180);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(adapter);



        //This was unused through out however the developer has sufficient knowledge in it
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }


    private void populateList() {
        // showing refresh animation before making http call
        swipeRefreshLayout.setRefreshing(true);

        //Make an http request to the url and obtain a response
        GetJson getJson = new GetJson("http://pastebin.com/raw/wgkJgazE");
        getJson.execute((Void) null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {
        imageLoader.clearAll();
        populateList();
    }

    private class GetJson extends AsyncTask<Void, String, Boolean> {


        String url;
        String json="";
        ServiceHandler serviceHandler;

        public GetJson(String url) {
            this.url = url;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            serviceHandler= new ServiceHandler(context);
        }

        @Override
        protected Boolean doInBackground(Void... voids) {

            try {
                json = serviceHandler.consume(url,"GET");
                return !TextUtils.isEmpty(json);
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if (aBoolean) {

                json = json.replace("\\u0026", "&");

                PersonItemRecyclerAdapter.persons =ParseResponseToObject(json);

                //Adapter has changed hence notify
                adapter.notifyDataSetChanged();
            }

            // stopping swipe refresh
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    /**
     * This method parses the response string to the List<Persons> object
     * This is the only method to attack if the response is XML and not JSON
     * @param json
     * @return
     */
    private List<Person> ParseResponseToObject(String json){
        Type listType = new TypeToken<List<Person>>() {
        }.getType();
        List<Person> persons = new Gson().fromJson(json, listType);

        return persons;
    }

    @Override
    public void onBackPressed() {
        if (mBackPressed + TIME_INTERVAL > System.currentTimeMillis()) {
            super.onBackPressed();
        } else {
            Toast.makeText(getApplicationContext(), "Press the Back button again to exit", Toast.LENGTH_SHORT).show();
            mBackPressed = System.currentTimeMillis();
        }
    }

    //The developer doesn't want the activity destroyed and recreated at every orientation changes
    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);
    }
}

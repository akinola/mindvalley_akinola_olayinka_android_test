package com.akinola.mindvalley_akinola_olayinka_android_test.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.akinola.imageloader.ImageLoader;
import com.akinola.mindvalley_akinola_olayinka_android_test.ActivityProfile;
import com.akinola.mindvalley_akinola_olayinka_android_test.MainActivity;
import com.akinola.mindvalley_akinola_olayinka_android_test.R;
import com.akinola.mindvalley_akinola_olayinka_android_test.models.Person;

import java.util.List;


/**
 * Created by Akinola.
 * This is the custom adapter for the persons being laid out in the recyclerview
 */
public class PersonItemRecyclerAdapter extends RecyclerView.Adapter<PersonItemRecyclerAdapter.RecyclerViewHolder> {
    private Context context;
    public static List<Person> persons;

    public PersonItemRecyclerAdapter(Context context) {
        this.context=context;

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflate the content_item_layout
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_item_layout,parent,false);
        RecyclerViewHolder recyclerViewHolder = new RecyclerViewHolder(v, context);
        return recyclerViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, final int position) {
        holder.txtPersonName.setText(persons.get(position).getUser().getName());
        holder.txtPersonLikes.setText(String.valueOf(persons.get(position).getLikes()));

        //holder.imgPersonThumb.setImageDrawable(null);

        MainActivity.imageLoader.withContext(context)
                .fromUrl(persons.get(position).getUser().getProfile_image().get("large"))
                .loadIntoView(holder.imgPersonThumb)
                .useProgressBar(holder.progressContent)
                .andCancelButton(holder.imgCancelRequest)
                .asFileType(ImageLoader.TYPE.IMAGE)
                .executeWithTag(persons.get(position).getId());

    }

    @Override
    public void onViewRecycled(RecyclerViewHolder holder) {
        super.onViewRecycled(holder);
        //imageLoader.clear(holder.imgPersonThumb);
    }

    @Override
    public int getItemCount() {
        return persons.size();
    }


    /**
     * A custom viewholder class
     */
    public class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        TextView txtPersonName;
        TextView txtPersonLikes;
        ImageView imgPersonThumb;
        ProgressBar progressContent;
        ImageView imgCancelRequest;
        Context mcontext;

        public RecyclerViewHolder(View itemView, Context context) {
            super(itemView);
            this.mcontext = context;

            txtPersonName = (TextView) itemView.findViewById(R.id.txtPersonName);
            txtPersonLikes = (TextView) itemView.findViewById(R.id.txtPersonLikes);
            imgPersonThumb = (ImageView)itemView.findViewById(R.id.imgPersonImage);
            imgCancelRequest = (ImageView)itemView.findViewById(R.id.imgCancelRequest);
            progressContent = (ProgressBar) itemView.findViewById(R.id.progressContent);



            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);

        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            Person p = persons.get(position);

            Intent i = new Intent(context, ActivityProfile.class);

            //We add Person p into the bundle. Person as well as all subclasses implement parcelable
            i.putExtra("PERSON",p);
            context.startActivity(i);

            //A simple animation to move to the new activity
            ((AppCompatActivity)context).overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);

        }

        @Override
        public boolean onLongClick(View v) {
            int position = getAdapterPosition();
            // TODO: 8/21/2016 If Long click is needed (Left intentionally)

            return true;
        }
    }
}


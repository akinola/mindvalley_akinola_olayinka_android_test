package com.akinola.mindvalley_akinola_olayinka_android_test.adapters;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

/**
 * @author AKINOLA
 *
 */
public class ServiceHandler {
	Context mContext;
	static InputStream is=null;

	public ServiceHandler(Context context) {

        this.mContext=context;
	}

    public String consume(String url,String method){

        String response = "";

        if (method.equalsIgnoreCase("GET")){
            response = consumeGet(url);
        }else if (method.equalsIgnoreCase("POST")){
            //response = consumePost(url);
        }
        return  response;
    }


    private String consumeGet(String url){
        String response = "";

        try {
            HttpURLConnection httpConnection = (HttpURLConnection)new URL(url).openConnection();
            httpConnection.setRequestMethod("GET");
            httpConnection.setReadTimeout(30000);
            httpConnection.setConnectTimeout(30000);
            httpConnection.setUseCaches(false);

            //getting the response code
            int responseCode = httpConnection.getResponseCode();
            System.out.println("Response code: "+responseCode);

            if (responseCode == HttpURLConnection.HTTP_OK) {

                //Reading the  Get response
                String line;
                BufferedReader br=new BufferedReader(new InputStreamReader(httpConnection.getInputStream()));
                while ((line=br.readLine()) != null) {
                    response+=line;
                }
            }else {
                response="";

            }
        }catch (IOException ex){
            ex.printStackTrace();
        }

        return response;
    }


    private String consumePost(String url, HashMap<String,String> postDataParams,String authorization){
        String response = "";
        try {
            HttpURLConnection httpConnection = (HttpURLConnection)new URL(url).openConnection();
            httpConnection.setDoInput(true);
            httpConnection.setDoOutput(true); // Triggers POST.
            httpConnection.setRequestMethod("POST");
            httpConnection.setReadTimeout(30000);
            httpConnection.setConnectTimeout(30000);
            httpConnection.setRequestProperty("Authorization", authorization);

            //encoding the requests
            String hh = getPostDataString(postDataParams);
            //System.out.println(hh);


            //writing the requests
            OutputStream os = httpConnection.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            writer.write(hh);
            writer.flush();
            writer.close();
            os.close();

            //getting the response code
            int responseCode = httpConnection.getResponseCode();
            System.out.println("Response code: "+responseCode);

            //if (responseCode == HttpURLConnection.HTTP_OK) {

            //Reading the response
            String line;
            BufferedReader br=new BufferedReader(new InputStreamReader(httpConnection.getInputStream()));
            while ((line=br.readLine()) != null) {
                response+=line;
            }


        }catch (IOException ex){
            ex.printStackTrace();
        }
        return response;

    }
	

    public String  performPostCall(String requestURL,HashMap<String, String> postDataParams) {

        URL url;
        String response = "";

        try {
            url = new URL(requestURL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(20000);
            conn.setConnectTimeout(20000);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");
            conn.setDoInput(true);
            conn.setDoOutput(true);

            String hh = getPostDataString(postDataParams);
            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            writer.write(hh);

            writer.flush();
            writer.close();
            os.close();


            int responseCode=conn.getResponseCode();
            Log.e("myhojj", "response: " + responseCode);

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                Date d1 = new Date();

                BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line=br.readLine()) != null) {
                    response+=line;
                }

                Date d2 = new Date();
                long elap = d2.getTime()-d1.getTime();

                Log.e("myhojj", "elapsed: " + elap);

            }
            else {
                response="";

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }
    private String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException{
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for(Map.Entry<String, String> entry : params.entrySet()){
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }

        return result.toString();
    }


    public String makeCall(String u,HashMap<String, String> params){
        String response="";
        try {
            URL url = new URL(u);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(20000);
            conn.setConnectTimeout(20000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);

            Uri.Builder builder = new Uri.Builder();

            for(Map.Entry<String, String> entry : params.entrySet()){
                builder.appendQueryParameter(entry.getKey(), entry.getValue());
            }


            String query = builder.build().getEncodedQuery();
            //Log.e("New Call", "query: " + query);

            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(query);
            writer.flush();
            writer.close();
            os.close();

            conn.connect();

            int responseCode=conn.getResponseCode();
            Log.i("New Call", "response: " + responseCode);

            if (responseCode == HttpURLConnection.HTTP_OK) {
                String line;
                //Date d1 = new Date();

                BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line=br.readLine()) != null) {
                    response+=line;
                }

                //Date d2 = new Date();
                //long elap = d2.getTime()-d1.getTime();

                //Log.e("New Call", "elapsed: " + elap);
                //Log.e("New Call", "response: " + response);

            }
            else {
                response="";

            }
            return response;
        }catch(Exception ex){
            ex.printStackTrace();
            return null;
        }

    }

}


















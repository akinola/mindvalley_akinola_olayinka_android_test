package com.akinola.mindvalley_akinola_olayinka_android_test.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Akinola on 8/20/2016.
 *
 */
public class CurrentUserCollections implements Parcelable {

    public CurrentUserCollections() {
    }

    protected CurrentUserCollections(Parcel in) {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<CurrentUserCollections> CREATOR = new Parcelable.Creator<CurrentUserCollections>() {
        @Override
        public CurrentUserCollections createFromParcel(Parcel in) {
            return new CurrentUserCollections(in);
        }

        @Override
        public CurrentUserCollections[] newArray(int size) {
            return new CurrentUserCollections[size];
        }
    };
}

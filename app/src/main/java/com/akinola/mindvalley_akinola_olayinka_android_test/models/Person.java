package com.akinola.mindvalley_akinola_olayinka_android_test.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Akinola Olayinka on 8/18/2016.
 * This is the model of a single Person
 */
public class Person implements Parcelable {

    private HashMap<String, String> urls;
    private String id = "";
    private String created_at = "";
    private int width;
    private int height;
    private String color = "";
    private int likes;
    private boolean liked_by_user;
    private User user;


    @SerializedName("current_user_collections")
    private List<CurrentUserCollections> currentUserCollections;

    private HashMap<String, String> links;

    private List<Categories> categories;

    public Person() {
        urls = new HashMap<>();
        links = new HashMap<>();
        user = new User();
        currentUserCollections = new ArrayList<>();
        categories = new ArrayList<>();
    }

    public HashMap<String, String> getUrls() {
        return urls;
    }

    public void setUrls(HashMap<String, String> urls) {
        this.urls = urls;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public boolean isLiked_by_user() {
        return liked_by_user;
    }

    public void setLiked_by_user(boolean liked_by_user) {
        this.liked_by_user = liked_by_user;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


    public HashMap<String, String> getLinks() {
        return links;
    }

    public void setLinks(HashMap<String, String> links) {
        this.links = links;
    }

    public List<Categories> getCategories() {
        return categories;
    }

    public void setCategories(List<Categories> categories) {
        this.categories = categories;
    }

    public List<CurrentUserCollections> getCurrentUserCollections() {
        return currentUserCollections;
    }

    public void setCurrentUserCollections(List<CurrentUserCollections> currentUserCollections) {
        this.currentUserCollections = currentUserCollections;
    }


    protected Person(Parcel in) {
        urls = (HashMap) in.readValue(HashMap.class.getClassLoader());
        id = in.readString();
        created_at = in.readString();
        width = in.readInt();
        height = in.readInt();
        color = in.readString();
        likes = in.readInt();
        liked_by_user = in.readByte() != 0x00;
        user = (User) in.readValue(User.class.getClassLoader());
        if (in.readByte() == 0x01) {
            currentUserCollections = new ArrayList<CurrentUserCollections>();
            in.readList(currentUserCollections, CurrentUserCollections.class.getClassLoader());
        } else {
            currentUserCollections = null;
        }
        links = (HashMap) in.readValue(HashMap.class.getClassLoader());
        if (in.readByte() == 0x01) {
            categories = new ArrayList<Categories>();
            in.readList(categories, Categories.class.getClassLoader());
        } else {
            categories = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(urls);
        dest.writeString(id);
        dest.writeString(created_at);
        dest.writeInt(width);
        dest.writeInt(height);
        dest.writeString(color);
        dest.writeInt(likes);
        dest.writeByte((byte) (liked_by_user ? 0x01 : 0x00));
        dest.writeValue(user);
        if (currentUserCollections == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(currentUserCollections);
        }
        dest.writeValue(links);
        if (categories == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(categories);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Person> CREATOR = new Parcelable.Creator<Person>() {
        @Override
        public Person createFromParcel(Parcel in) {
            return new Person(in);
        }

        @Override
        public Person[] newArray(int size) {
            return new Person[size];
        }
    };
}

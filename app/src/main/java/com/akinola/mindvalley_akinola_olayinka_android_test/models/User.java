package com.akinola.mindvalley_akinola_olayinka_android_test.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;

/**
 * Created by Akinola on 8/19/2016.
 *
 */
public class User implements Parcelable {

    private String id= "";
    private String username= "";
    private String name= "";
    private HashMap<String,String> profile_image;
    private HashMap<String,String> links;

    public User() {
        profile_image = new HashMap<>();
        links = new HashMap<>();
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public HashMap<String, String> getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(HashMap<String, String> profile_image) {
        this.profile_image = profile_image;
    }

    public HashMap<String, String> getLinks() {
        return links;
    }

    public void setLinks(HashMap<String, String> links) {
        this.links = links;
    }

    protected User(Parcel in) {
        id = in.readString();
        username = in.readString();
        name = in.readString();
        profile_image = (HashMap) in.readValue(HashMap.class.getClassLoader());
        links = (HashMap) in.readValue(HashMap.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(username);
        dest.writeString(name);
        dest.writeValue(profile_image);
        dest.writeValue(links);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
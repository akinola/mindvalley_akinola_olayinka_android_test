package com.akinola.imageloader;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.LruCache;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Akinola on 8/18/2016.
 * Asynchronous resource loader class
 */
public final class ImageLoader {
    private String tag;

    private ImageView imageView;
    private ProgressBar progressBar;
    private ImageView cancelButton;

    private static Bitmap onCancelledImage;
    private static Bitmap onErrorImage;

    private String url;


    private static ImageLoader imageLoader;
    private static List<String> cancelledTags;
    private static HashMap<String, ContentLoader> contentLoaders;

    private Context context;
    private static LruCache<String, byte[]> mDocsMemoryCache;
    final String TAG = "MindValley";
    private static OkHttpClient client;

    private TYPE type;

    public enum TYPE {
        IMAGE, JSON
    }

    //We want a single instance of the Imageloader always
    public static synchronized ImageLoader getInstance(int cacheSize) {
        if (imageLoader == null) {
            //Create an instance if null
            imageLoader = new ImageLoader(cacheSize);
        }
        return imageLoader;
    }


    /**
     * Imageloader constructor class. It takes an input of the cache size.
     * Max cache size is taken as one-eight of the available memory
     * @param cacheSize
     */
    private ImageLoader(int cacheSize) {
        client = new OkHttpClient();
        cancelledTags = new ArrayList<>();
        contentLoaders = new HashMap<>();

        // Get max available VM memory, exceeding this amount will throw an
        // OutOfMemory exception. Stored in kilobytes as LruCache takes an
        // int in its constructor.
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

        // Use 1/8th of the available memory for the image memory cache.
        int maxCacheSize = maxMemory / 8;

        if (cacheSize > maxCacheSize) {
            cacheSize = maxCacheSize;
        }


        /**
         * Use another 1/8th of the available memory for bytes storage memory cache.
         * byte[] array used so that other filetypes can be handled
         */
        mDocsMemoryCache = new LruCache<String, byte[]>(cacheSize) {
            @Override
            protected int sizeOf(String key, byte[] value) {
                // The cache size will be measured in kilobytes rather than
                // number of items.
                return value.length / 1024;
            }
        };
    }


    /**
     * Designate an ImageView to load bitmap into
     * Required for Image TYPE
     * @param imageView
     * @return
     */
    public ImageLoader loadIntoView(ImageView imageView) {
        this.imageView = imageView;
        return this;
    }

    /**
     * Optional
     * Designate a progressbar to show while loading     *
     * @param progressBar
     * @return
     */
    public ImageLoader useProgressBar(ProgressBar progressBar) {
        this.progressBar = progressBar;
        return this;
    }

    /**
     * Optional
     * Designate an imageview to use as cancel button
     * @param cancelButton
     * @return
     */
    public ImageLoader andCancelButton(ImageView cancelButton) {
        this.cancelButton = cancelButton;
        return this;
    }

    /**
     * Optional
     * Designate an bitmap to use if download is cancelled to use as cancel button
     * @param onCancelledImage: cancelled bitmap to use if download is cancelled
     * @return
     */
    public ImageLoader useCancelledBitmap(Bitmap onCancelledImage) {
        ImageLoader.onCancelledImage = onCancelledImage;
        return this;
    }


    /**
     * Optional
     * Designate an bitmap to use if download is cancelled to use as cancel button
     * @param onErrorImage : error bitmap to use if download fails
     * @return
     */
    public ImageLoader useErrorBitmap(Bitmap onErrorImage) {
        ImageLoader.onErrorImage = onErrorImage;
        return this;
    }

    /**
     * Required
     * Url to load byte[] from
     * @param url
     * @return
     */
    public ImageLoader fromUrl(String url) {
        this.url = url;
        return this;
    }

    /**
     * Tag is necessary for the cancel button onClickevent
     * @param tag
     * @return
     */
    public ImageLoader withTag(String tag) {
        this.tag = tag;
        return this;
    }

    /**
     * Required
     * Specify TYPE to use, Default type is IMAGE
     * @param fileType
     * @return
     */
    public ImageLoader asFileType(TYPE fileType) {
        type = fileType;
        return this;
    }

    /**
     *
     * @return
     */
    public static HashMap<String, ContentLoader> getContentLoaders() {
        return contentLoaders;
    }

    public static void setContentLoaders(HashMap<String, ContentLoader> contentLoaders) {
        ImageLoader.contentLoaders = contentLoaders;
    }

    /**
     * Context
     * @param context
     * @return
     */
    public ImageLoader withContext(Context context) {
        this.context = context;
        return this;
    }


    /**
     * The content loader class
     * This class obtains a new byte[] if not found in Memorycache but resuses the existing one
     * in cache if it is found
     */
    private class ContentLoader {
        byte[] out;
        ImageView imageView1;
        ProgressBar progressBar1;
        ImageView cancelButton1;
        TYPE type1;
        String url1;
        public String tag1;
        boolean canRefresh = false;

        public ContentLoader(ImageView imageView, ProgressBar progressBar, ImageView cancelButton, TYPE type, String url, String tag) {
            this.imageView1 = imageView;
            this.progressBar1 = progressBar;
            this.cancelButton1 = cancelButton;
            this.type1 = type;
            this.url1 = url;
            this.tag1 = tag;

            this.imageView1.setTag("1: "+this.tag1);

        }

        public String getTag1() {
            return tag1;
        }

        public void setTag1(String tag1) {
            this.tag1 = tag1;
        }

        public void execute() {
            if (type1 == null) {
                type1=TYPE.IMAGE;
            }

            if (cancelledTags.contains(tag1)) {
                if (imageView1 != null) {
                    if (onCancelledImage!=null){
                        imageView1.setImageBitmap(onCancelledImage);
                    }else{
                        imageView1.setImageDrawable(null);
                    }

                    canRefresh = true;
                    if (progressBar1 != null) {
                        progressBar1.setVisibility(View.GONE);
                    }
                    if (cancelButton1 != null) {
                        cancelButton1.setImageResource(android.R.drawable.ic_menu_rotate);
                        cancelButton1.setVisibility(View.VISIBLE);
                    }

                    Log.e(TAG, "The cancelled tag is " + tag1 + " so when we reload it should be the sahe");

                    return;
                }
            }

            out = getBytesFromMemCache(this.url1);

            switch (type1) {
                case IMAGE:
                    if (imageView1 == null) {
                        throw new NullPointerException("Imageview is null");
                    }

                    //refresh
                    Log.e(TAG, "Here checking 1" + tag1);

                    if (out == null) {
                        // if asked thumbnail is not present it will be put into cache
                        //Log.e(TAG, "We downloaded bitmap afresh");

                        if (this.cancelButton1 != null) {
                            this.cancelButton1.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    if (canRefresh) {
                                        if (progressBar1 != null) {
                                            progressBar1.setVisibility(View.VISIBLE);
                                        }
                                        cancelButton1.setVisibility(View.VISIBLE);
                                        cancelButton1.setImageResource(android.R.drawable.ic_delete);

                                        //refresh
                                        Log.e(TAG, "reload tag " + tag1);
                                        reload(getTag1());
                                    } else {
                                        if (progressBar1 != null) {
                                            progressBar1.setVisibility(View.GONE);
                                        }

                                        canRefresh = true;
                                        cancelButton1.setImageResource(android.R.drawable.ic_menu_rotate);
                                        cancelButton1.setVisibility(View.VISIBLE);
                                        Log.e(TAG, "cancel tag " + tag1);
                                        cancelLoad(getTag1());
                                    }
                                }
                            });
                        }


                        Request request = new Request.Builder()
                                .url(this.url1)
                                .tag(tag1)
                                .build();


                        client.newCall(request).enqueue(new Callback() {


                            @Override
                            public void onFailure(Call call, IOException e) {
                                //Log.e(TAG, "download failed " + tag1);

                                ((AppCompatActivity) context).runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (onErrorImage!=null){
                                            imageView1.setImageBitmap(onErrorImage);
                                        }else{
                                            imageView1.setImageDrawable(null);
                                        }

                                        if (progressBar1 != null) {
                                            progressBar1.setVisibility(View.GONE);
                                        }
                                        canRefresh = true;
                                        if (cancelButton1 != null) {
                                            cancelButton1.setImageResource(android.R.drawable.ic_menu_rotate);
                                        }
                                    }
                                });

                            }

                            @Override
                            public void onResponse(Call call, Response response) throws IOException {
                                //Log.e(TAG, "download passed " + tag1);
                                byte[] output = response.body().bytes();
                                addBytesToMemoryCache(url1, output);
                                final Bitmap bitmap = BitmapFactory.decodeByteArray(output, 0, output.length);

                                ((AppCompatActivity) context).runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        imageView1.setImageBitmap(bitmap);
                                        if (progressBar1 != null) {
                                            progressBar1.setVisibility(View.GONE);
                                        }

                                        canRefresh = false;
                                        if (cancelButton1 != null) {
                                            cancelButton1.setVisibility(View.GONE);
                                        }
                                    }
                                });


                            }
                        });

                    } else {
                        Bitmap b = BitmapFactory.decodeByteArray(out, 0, out.length);
                        //we fetch it from cache
                        Log.e(TAG, "Here checking 2" + tag1);

                        this.setTag1(tag1);
                        imageView1.setTag("1 "+tag1);
                        imageView1.setImageBitmap(b);
                        imageView1.invalidate();

                        if (progressBar1 != null) {
                            progressBar1.setVisibility(View.GONE);
                        }

                        canRefresh = false;
                        if (cancelButton1 != null) {
                            cancelButton1.setVisibility(View.GONE);
                        }
                    }

                    break;
                default:
                    if (out == null) {
                        // if asked thumbnail is not present it will be put into cache
                        Log.e(TAG, "We downloaded bytes afresh");
                        Request request = new Request.Builder()
                                .url(this.url1)
                                .tag(tag1)
                                .build();


                        client.newCall(request).enqueue(new Callback() {


                            @Override
                            public void onFailure(Call call, IOException e) {
                                Log.e(TAG, "download failed " + tag1);

                            }

                            @Override
                            public void onResponse(Call call, Response response) throws IOException {
                                Log.e(TAG, "download passed " + tag1);
                                byte[] output = response.body().bytes();
                                addBytesToMemoryCache(url1, output);
                            }
                        });

                    } else {
                        //we fetch it from cache
                        Log.e(TAG, "We obtained bytes from cache");
                        String v = new String(out);
                        Log.e(TAG, "Tostring: " + v);
                    }
            }
        }
    }

    /**@Deprecated
     * Executes the content Loader class
     */
    public void execute() {
        ContentLoader loader = new ContentLoader(this.imageView, this.progressBar, this.cancelButton, this.type, this.url, this.tag);
        loader.execute();
    }

    /**
     * Executes the content Loader class, checks if an instance of the loader class exists
     * Execute if present
     * Create a new instance and execute of abscent
     * @param tag
     */
    public void executeWithTag(String tag) {

        if (!contentLoaders.containsKey(tag)) {
            ContentLoader loader = new ContentLoader(this.imageView, this.progressBar, this.cancelButton, this.type, this.url, tag);
            loader.execute();
            contentLoaders.put(tag, loader);

        } else {

            ContentLoader loader = contentLoaders.get(tag);
            Log.e(TAG, "Loader is present so we do nothing : "+loader.getTag1());
            loader.execute();
        }

    }

    /**
     * Clear all the content loaders
     * Clear all Tags
     */
   public void clearAll(){
       contentLoaders.clear();
       cancelledTags.clear();
   }





    public void clear(ImageView imageView) {
        imageView.setImageDrawable(null);
    }

    /**
     * If cancelbutton is provided, then on refresh this handles the refresh
     * @param tag
     */
    private void reload(String tag) {
        if (cancelledTags.contains(tag)) {
            cancelledTags.remove(tag);
        }
        if (contentLoaders.containsKey(tag)) {
            ContentLoader loader = contentLoaders.get(tag);
            loader.execute();
        }

    }


    /**
     * This method adds the byte[] to memory with a key if it isn't present
     * @param key
     * @param value
     */
    private void addBytesToMemoryCache(String key, byte[] value) {
        if (getBytesFromMemCache(key) == null) {
            mDocsMemoryCache.put(key, value);
        }
    }


    /**
     * Get byte[] from memorycache by key.
     * returns null if absent
     * @param key
     * @return
     */
    public byte[] getBytesFromMemCache(String key) {
        return mDocsMemoryCache.get(key);
    }

    /**
     * Cancel a currently loading or queued request by its tag
     * @param tag
     */
    private void cancelLoad(String tag) {
        OkHttpUtils.cancelCallWithTag(client, tag);
        if (!cancelledTags.contains(tag)) {
            cancelledTags.add(tag);
        }
    }


}

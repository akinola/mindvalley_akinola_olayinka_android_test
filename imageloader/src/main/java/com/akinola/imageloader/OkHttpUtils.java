package com.akinola.imageloader;

import android.util.Log;

import okhttp3.Call;
import okhttp3.OkHttpClient;

/**
 * Created by Akinola on 8/20/2016.
 * Thanks to stack overflow answers
 *
 */
public class OkHttpUtils {
    public static void cancelCallWithTag(OkHttpClient client, String tag) {
        for(Call call : client.dispatcher().queuedCalls()) {
            if(call.request().tag().equals(tag)) {
                call.cancel();
            }
        }
        for(Call call : client.dispatcher().runningCalls()) {
            if(call.request().tag().equals(tag)) {
                call.cancel();
            }
        }
    }
}

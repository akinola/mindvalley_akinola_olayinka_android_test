package com.akinola.imageloader;

import android.content.Context;
import android.util.LruCache;
import android.widget.ImageView;
import android.widget.ProgressBar;

import junit.framework.Assert;
import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by User on 8/21/2016.
 */
public class ImageLoaderTest extends TestCase {


    private LruCache<String, byte[]> mDocsMemoryCache;
    private static List<String> cancelledTags;
    private Context context;

    @Override
    public void setUp() throws Exception {
        super.setUp();


        cancelledTags = new ArrayList<>();
        int cacheSize = 10000;
        ImageLoader imageLoader = ImageLoader.getInstance(cacheSize);

        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        int maxCacheSize = maxMemory / 8;

        if (cacheSize > maxCacheSize) {
            cacheSize = maxCacheSize;
        }

        mDocsMemoryCache = new LruCache<String, byte[]>(cacheSize) {
            @Override
            protected int sizeOf(String key, byte[] value) {
                return value.length / 1024;
            }
        };

    }

    public void testImageLoader(){

    }

    public void testContentLoaders(){
        assertFalse(ImageLoader.getContentLoaders()==null);
    }

    public void testMemCache(){
        assertFalse(mDocsMemoryCache==null);
    }



    @Override
    public void tearDown() throws Exception {
        super.tearDown();
    }
}

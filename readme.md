This is an asychronous resource loader class for the MindValley Test.

How To Use the library:

Instantiate the ImageLoader
    int cacheSize = 10000; //At the user's discretion
    ImageLoader imageLoader = ImageLoader.getInstance(cacheSize);
    
Instantiate the following:
    1. The Imageview into which an a bitmap is to be loaded
            ImageView mImageView = (ImageView)findViewById(R.id.imageView);
            
    2. The Url (string), the Context, a unique identifier tag (String) and the content type (ImageLoader.TYPE.IMAGE, ImageLoader.TYPE.JSON etc)
        String url = "http://myurl.com";
        Context context = getApplicationContext();
        String tag = "uniquetTag"
        
    3. Execute the ImageLoader as thus:
    
            imageLoader.withContext(context)
                    .fromUrl(url)
                    .loadIntoView(mImageView)
                    .withTag(tag)
                    .asFileType(ImageLoader.TYPE.IMAGE)
                    .execute();
                    
                    
    4. You can specify a ProgressBar and Cancel button (ImageView) to use while loading an image
            ImageView mImageViewCancel = (ImageView)findViewById(R.id.imageViewCancel);
            ProgressBar mProgressBar = (ProgressBar)findViewById(R.id.progressBar);
            
            imageLoader.withContext(context)
                .fromUrl(url)
                .loadIntoView(mImageView)
                .useProgressBar(mProgressBar)
                .andCancelButton(mImageViewCancel)
                .asFileType(ImageLoader.TYPE.IMAGE)
                .executeWithTag(tag);
                
    5. The Library caches to memory using the LruCache library
    
    
    6. The maximum cache available to a user was set at one-eight of the available runtime cache to prevent OutOfMemoryException
    
    
    7. Credit to OkHttp3 Library which was used to handle many Http Requests.
    
    
    8. Enjoy.    
        
        
        
        
        
        
    